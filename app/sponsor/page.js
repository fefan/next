import getData from "@/api";
import styles from "./style.module.scss";
import Link from "next/link";
import Empty from "@/components/empty";

export async function generateMetadata() {
    return {
        metadataBase: `${process.env.NEXT_PUBLIC_ORIGIN}`,
        title: "Partenaires",
    }
}


export default async function Sponsor() {
    const site = await getData('site', {
        populate: {
            edition: {
                fields: [],
                populate: {
                    sponsors: {
                        fields: ['name', 'uri', 'image'],
                        populate: {
                            image: {
                                fields: ['alternativeText', 'name', 'url']
                            }
                        }
                    }
                }
            }
        }
    });
    const edition = site.data?.attributes.edition.data.attributes;
    const sponsors = edition?.sponsors.data;

    return (
        <main className={styles.main}>
            <h2>Ils nous soutiennent</h2>
            {
                sponsors && sponsors.length > 0 ? 
                <section className={styles.sponsors}>
                {
                    sponsors.map(({id, attributes: attr}) => {
                        return (
                            <figure key={id}>
                                <Link 
                                    target="_blank"
                                    href={attr.uri}
                                >
                                    <img
                                        alt={attr.image.data.attributes.alternativeText} 
                                        src={`${process.env.NEXT_PUBLIC_IMG_URI}${attr.image.data.attributes.url}`}
                                    />
                                </Link>
                                <figcaption>
                                    <Link 
                                        target="_blank"
                                        href={attr.uri}
                                    >
                                        {attr.name}
                                    </Link>
                                </figcaption>
                            </figure>
                        );
                    })
                }
                </section> : <Empty message="Pas de sponsors à afficher pour l'édition en cours"/>
            }
        </main>
    );
}