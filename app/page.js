import getData from '@/api';
import styles from './page.module.scss'
import { BlocksRenderer } from '@strapi/blocks-react-renderer';
import Button from '@/components/button';
import ImageBlock from '@/components/imageBlock';

export async function generateMetadata() {
    const site = await getData('site', {
        populate: {
            edition: {
                populate: {
                    flyer: {
                        fields: ['name', 'url', 'alternativeText', 'caption', 'width', 'height']
                    },
                    programs: true
                },
                fields: ['id', 'introduction', 'title', 'subtitle', 'description'],
            }
        },
        fields: ['author']
    });
    const current_edition = site.data?.attributes.edition.data?.attributes;
    const flyer = site.data?.attributes.edition.data?.attributes.flyer.data.attributes;

    return current_edition ? {
        metadataBase: `${process.env.NEXT_PUBLIC_ORIGIN}`,
        title: current_edition.title,
        description: current_edition.description,
        alternates: {
            canonical: '/',
        },
        openGraph: {
            title: current_edition.title,
            url: `${process.env.NEXT_PUBLIC_ORIGIN}`,
            description: current_edition.description, 
            images: {
                url: `${process.env.NEXT_PUBLIC_IMG_URI}${flyer.url}`,
                width: flyer.width,
                height: flyer.height,
            },
            authors: [site.data.attributes.author],
            type: 'website',
            locale: 'fr_FR',
            siteName: 'Le Fefan - Festival de Fanfares'
        }
    } : {}
}

export default async function Home() {
    const site = await getData('site', {
        populate: {
            edition: {
                populate: {
                    flyer: {
                        fields: ['name', 'url', 'alternativeText', 'caption']
                    },
                    programs: true
                },
                fields: ['id', 'introduction', 'title', 'subtitle'],
            }
        }
    });
    const current_edition = site.data?.attributes.edition.data?.attributes;
    const flyer = site.data?.attributes.edition.data?.attributes.flyer.data.attributes;
    const introduction = current_edition?.introduction;
    const programs = current_edition?.programs.data;

    return (
        <main className={styles.main}>
            {
                current_edition ? 
                <section className={styles.home}>
                    {
                        flyer ? <ImageBlock
                            className={styles.flyer}
                            alt={flyer.alternativeText}
                            src={`${process.env.NEXT_PUBLIC_IMG_URI}${flyer.url}`} 
                        /> : null
                    }
                    <article>
                        <h2>{current_edition.title}</h2>
                        <h3>{current_edition.subtitle}</h3>
                        {introduction ? <BlocksRenderer content={introduction} /> : null}
                        <nav>
                           { 
                                programs.filter((e) => e.attributes.type === "city-wide").length > 0 ? <Button 
                                    as="a"
                                    type="primary"
                                    href="/prog/city-wide"
                                >Voir la programmation</Button> : null
                            }
                            {
                                programs.filter((e) => e.attributes.type === "village").length > 0 ?
                                <Button 
                                    as="a"
                                    type="secondary"
                                    href="/prog/village"
                                >Le Village</Button> : null
                            }
                        </nav>
                    </article>
                </section>
                : null
            }
        </main>
    )
}