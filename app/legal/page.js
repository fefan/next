import getData from "@/api";
import { BlocksRenderer } from "@strapi/blocks-react-renderer";
import styles from "./style.module.scss";

export async function generateMetadata() {
    return {
        metadataBase: `${process.env.NEXT_PUBLIC_ORIGIN}`,
        title: "Mentions légales",
    }
}

export default async function Legal() {
    const site = await getData('site', {})
    const content = site.data?.attributes.legal

    return (
        <main className={styles.main}>
            <article>
                {content ? <BlocksRenderer content={content}/> : null}
            </article>
        </main>
    );
}