import getData from '@/api';

export default async function sitemap() {
    // Main page information
    const site = await getData('site', {
        fields: ['updatedAt'],
        populate: {
            edition: {
                fields: ['id', 'updatedAt', 'publishedAt'],
                populate: {
                    programs: true,
                    fields: ['type', 'updatedAt', 'publishedAt']
                }
            },
        },
    });

    const edition = site.data?.attributes.edition.data;
    const siteLastModified = site && edition ? new Date(Math.max(
        new Date(site.data?.attributes.updatedAt),
        new Date(edition.attributes.updatedAt),
    )) : new Date();

    // Programs information
    const programs = (edition?.attributes.programs.data ?? []).filter(p => p.attributes.publishedAt !== null) ;

    // Editions information
    const editions = await getData('editions', { fields: ['id', 'updatedAt'] });
    const previousEditions = editions.data?.filter(e => e.id !== edition.id && e.attributes.publishedAt !== null);
    const editionsLastModifiedTimestamp = Math.max(
        ...(previousEditions ?? []).map(e => new Date(e.attributes.updatedAt))
    );

    const editionsLastModified = editionsLastModifiedTimestamp === -Infinity ? siteLastModified : new Date(editionsLastModifiedTimestamp);

    return [
        {
            url: `${process.env.NEXT_PUBLIC_ORIGIN}`,
            lastModified: siteLastModified,
            changeFrequency: 'yearly',
            prority: 1,
        },
        ...(programs ?? []).map(p => ({
            url: `${process.env.NEXT_PUBLIC_ORIGIN}/prog/${p.attributes.type}`,
            lastModified: new Date(p.attributes.updatedAt),
            changeFrequency: 'yearly',
            priority: 0.8,
        })),
        {
            url: `${process.env.NEXT_PUBLIC_ORIGIN}/editions`,
            lastModified: editionsLastModified,
            changeFrequency: 'yearly',
            priority: 0.6,
        },
        ...(previousEditions ?? []).map(e => ({
            url: `${process.env.NEXT_PUBLIC_ORIGIN}/editions/${e.id}`,
            lastModified: new Date(e.attributes.updatedAt),
            changeFrequency: 'yearly',
            priority: 0.5,
        })),
    ];
}
