import Empty from '@/components/empty';
import styles from './style.module.scss'
import getData from '@/api';
import ImageBlock from '@/components/imageBlock';
import { BlocksRenderer } from '@strapi/blocks-react-renderer';
import TimeSlot from '@/components/timeSlot';

export async function generateMetadata() {
    const site = await getData('site', {
        populate: {
            edition: {
                populate: {
                    flyer: {
                        fields: ['name', 'url', 'alternativeText', 'caption', 'width', 'height']
                    },
                    programs: true
                },
                fields: ['id', 'introduction', 'title', 'subtitle'],
            }
        },
        fields: ['author']
    });
    const current_edition = site.data?.attributes.edition.data?.attributes;
    const flyer = site.data?.attributes.edition.data?.attributes.flyer.data.attributes;
    const programs = current_edition?.programs.data;
    const village = programs?.filter((e) => e.attributes.type === "village")[0]

    return village ? {
        metadataBase: `${process.env.NEXT_PUBLIC_ORIGIN}`,
        title: village.attributes.title,
        description: village.attributes.description,
        alternates: {
            canonical: '/prog/village',
        },
        openGraph: {
            title: village.attributes.title,
            url: `${process.env.NEXT_PUBLIC_ORIGIN}/prog/village`,
            description: village.attributes.description, 
            images: {
                url: `${process.env.NEXT_PUBLIC_IMG_URI}${flyer.url}`,
                width: flyer.width,
                height: flyer.height,
            },
            authors: [site.data.attributes.author],
            type: 'website',
            locale: 'fr_FR',
            siteName: 'Le Fefan - Festival de Fanfares'
        }
    } : {}
}

export default async function Village() {
    const site = await getData('site', {
      populate: {
          edition: {
              populate: {
                  programs: {
                      populate: {
                          sticker: {
                            fields: ['alternativeText', 'url']
                          },
                          bands: {
                              fields: ['*']
                          },
                          time_slots: {
                              fields: ['*']
                          },
                        }
                  }
              }
          }
      }
  });
  const edition = site.data?.attributes.edition ?? {}
  const program = edition.data?.attributes.programs.data.filter(({attributes: r}) => r.type=== "village")[0] ?? {}
  const time_slots = program.attributes?.time_slots.data ?? [];

  return (
      <main className={styles.main}>
        {program.attributes ? 
        <>
            <h2>{program.attributes?.title ?? "Le Village"}</h2>
            <section className={styles.villageSection}>
                <article>
                    {
                        program.attributes?.introduction ? 
                        <BlocksRenderer content={program.attributes.introduction}/>
                        : null
                    }
                </article>
                {
                    program.attributes?.sticker?.data ? 
                    <ImageBlock 
                        src={`${process.env.NEXT_PUBLIC_IMG_URI}${program.attributes.sticker.data.attributes.url}`}
                        alt={program.attributes.sticker.data.attributes.alternativeText}
                        className={styles.villageImg}
                    /> : <div></div>
                }
                <article>
                {
                    time_slots.map(({id, attributes: attr}) => {
                        return (
                            <TimeSlot
                                key={id}
                                content={attr.content}
                                start={attr.start}
                                end={attr.end}
                            />
                        );
                    })
                }
                </article>
            </section>
        </> : <Empty message="Le village est encore secret"></Empty>}
      </main>
  )
}