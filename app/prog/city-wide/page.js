import getData from '@/api';
import styles from './style.module.scss'
import Fanfare from '@/components/fanfare';
import TimeSlot from '@/components/timeSlot';
import Empty from '@/components/empty';

export async function generateMetadata() {
    const site = await getData('site', {
        populate: {
            edition: {
                populate: {
                    flyer: {
                        fields: ['name', 'url', 'alternativeText', 'caption', 'width', 'height']
                    },
                    programs: true
                },
                fields: ['id', 'introduction', 'title', 'subtitle'],
            }
        },
        fields: ['author']
    });
    const current_edition = site.data?.attributes.edition.data?.attributes;
    const flyer = site.data?.attributes.edition.data?.attributes.flyer.data.attributes;
    const programs = current_edition?.programs.data;
    const cityWide = programs?.filter((e) => e.attributes.type === "city-wide")[0]

    return cityWide ? {
        metadataBase: `${process.env.NEXT_PUBLIC_ORIGIN}`,
        title: cityWide.attributes.title,
        description: cityWide.attributes.description,
        alternates: {
            canonical: '/prog/city-wide',
        },
        openGraph: {
            title: cityWide.attributes.title,
            url: `${process.env.NEXT_PUBLIC_ORIGIN}/prog/city-wide`,
            description: cityWide.attributes.description, 
            images: {
                url: `${process.env.NEXT_PUBLIC_IMG_URI}${flyer.url}`,
                width: flyer.width,
                height: flyer.height,
            },
            authors: [site.data.attributes.author],
            type: 'website',
            locale: 'fr_FR',
            siteName: 'Le Fefan - Festival de Fanfares'
        }
    } : {}
}

export default async function CityWide() {
    const site = await getData('site', {
        populate: {
            edition: {
                populate: {
                    programs: {
                        populate: {
                            bands: {
                                fields: ['*']
                            },
                            time_slots: {
                                fields: ['*']
                            }
                        }
                    }
                }
            }
        }
    });
    const edition = site.data?.attributes.edition ?? {}
    const program = edition.data?.attributes.programs.data.filter(({attributes: r}) => r.type=== "city-wide")[0] ?? {}
    const time_slots = program.attributes?.time_slots.data ?? [];

    return (

        <main className={styles.main}>
            { program.attributes ? <>
                <h2>{program.attributes.title}</h2>
                <section className={styles.program}>
                    {
                        time_slots.map(({id, attributes: attr}) => {
                            return (
                                <TimeSlot 
                                    key={id}
                                    content={attr.content}
                                    start={attr.start}
                                    end={attr.end}
                                />
                            );
                        })
                    }
                </section>
                <section className={styles.program}>
                    <article className={styles.featuring}>
                        { program.attributes.bands.data.length === 0 ? null : <h3>Les Fanfares</h3>}
                        <p className={styles.introduction}>{program.attributes.description}</p>
                        {
                        program.attributes.bands.data.map(({id, attributes: attr}) => {
                            return (
                                <Fanfare 
                                    key={id}
                                    location={attr.location}
                                    name={attr.name}
                                />
                            );
                        })
                        }
                    </article>
                    <iframe className={styles.map} src={program.attributes.map_uri} width={640} height={480}></iframe>
                </section>
                
                <section className={styles.program}></section>
            </> : <Empty message="Pas de programmation à afficher pour le moment"></Empty>}
        </main>
    )
}
