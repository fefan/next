import getData from '@/api';
import styles from './style.module.scss';
import EditionElement from '@/components/editionElement';
import EditionGallery from '@/components/editionGallery';
import PressBlock from '@/components/pressBlock';
import Empty from '@/components/empty';
import VideoBlock from '@/components/videoBlock';
import Fanfare from '@/components/fanfare';

export async function generateMetadata({params}) {
    const data = await getData(`editions/${params.editionId}`, {
        populate: {
            flyer: {
                fields: ['name', 'alternativeText', 'caption', 'url']
            },
        },
        filters: {
            $or: [
                { id: { $eq: params.editionId } },
            ]
        }
    })
    const activeEditionData = await getData('site', {
        populate: {
            edition: {
                fields: ['id']
            }
        },
        fields: ['author']
    });
    const activeEdition = activeEditionData?.data?.attributes.edition.data.id;
    const edition = data.data?.attributes.publishedAt ? data : null;
    const flyer = edition?.data?.attributes.flyer.data.attributes;

    return edition ? {
        metadataBase: `${process.env.NEXT_PUBLIC_ORIGIN}`,
        title: edition.title,
        description: edition.description,
        alternates: {
            canonical: data.data.id !== activeEdition ? `${process.env.NEXT_PUBLIC_ORIGIN}/editions/${params.editionId}` : `${process.env.NEXT_PUBLIC_ORIGIN}`,
        },
        openGraph: {
            title: edition.title,
            url: data.data.id !== activeEdition ? `${process.env.NEXT_PUBLIC_ORIGIN}/editions/${params.editionId}` : `${process.env.NEXT_PUBLIC_ORIGIN}`,
            description: edition.description, 
            images: {
                url: `${process.env.NEXT_PUBLIC_IMG_URI}${flyer.url}`,
                width: flyer.width,
                height: flyer.height,
            },
            authors: [activeEditionData.data.attributes.author],
            type: 'website',
            locale: 'fr_FR',
            siteName: 'Le Fefan - Festival de Fanfares'
        }
    } : {}
}

export default async function Edition({params}) {
    const data = await getData(`editions/${params.editionId}`, {
        populate: {
            flyer: {
                fields: ['name', 'alternativeText', 'caption', 'url']
            },
            gallery: {
                fields: ['name', 'alternativeText', 'caption', 'url']
            },
            programs: {
                fields: ['map_uri', 'introduction', 'description', 'title', 'type'],
                populate: {
                    bands: {
                        fields: ['name', 'location']
                    }
                }
            },
            statistics: {
                fields: ['name', 'value', 'publishedAt']
            },
            social_links: {
                fields: ['uri', 'type']
            },
            articles: {
                fields: ['title', 'link', 'excerpt', 'publishedAt']
            },
            fields: ['movie']
        },
        filters: {
            $or: [
                { id: { $eq: params.editionId } },
            ]
        }
    })
    const edition = data.data?.attributes.publishedAt ? data : null;
    const flyer = edition?.data?.attributes.flyer.data.attributes;
    const gallery = edition?.data?.attributes.gallery.data;
    const statistics = edition?.data?.attributes.statistics.data;
    const allArticles = edition?.data?.attributes.articles.data;
    const articles = allArticles ? allArticles.filter(el => el.attributes.publishedAt != null) : [];
    const movie =  edition.data.attributes.movie ? edition.data.attributes.movie : null;
    const programs = edition?.data?.attributes?.programs?.data ? edition.data.attributes.programs.data : [];
    const program = programs.filter((el) => el.attributes.type ==='city-wide')[0] ?? null;
    const bands = program ? program?.attributes?.bands.data : [];

    return (
        <main className={styles.main}>
            {
                edition ?
                <>
                <h2>{ edition.data.attributes.title }</h2>
                <h3>{ edition.data.attributes.subtitle }</h3>
                <EditionElement
                    flyerImg={`${process.env.NEXT_PUBLIC_IMG_URI}${flyer.url}`}
                    flyerAlt={flyer.alternativeText}
                    blocks={statistics.map(({id, attributes}) => ({id, type: 'stat', title: attributes.name, value: attributes.value}))}
                />
                {
                    movie && bands ?
                    <section className={styles.smallProgram}>
                        {
                            bands.length > 0 ?
                            <article className={styles.featuring}>
                            <h4>Les fanfares</h4>
                            {
                                bands.map(({id, attributes: attr}) => {
                                    return  <Fanfare 
                                    key={id}
                                    location={attr.location}
                                    name={attr.name}
                                    />
                                })
                            }
                            </article> : null
                        }
                        {
                            movie ? 
                            <>
                                <VideoBlock
                                    src={movie}
                                    title={`Aftermovie du festival ${edition.data.attributes?.title ?? ""}`}
                                />
                            </> : null
                        }
                    </section> : null
                }
                {
                    articles.length > 0 ?
                    <>
                        <h4>Ils ont parlé de nous !</h4>
                        {

                            articles.map(({id, attributes}, index) => {
                                const offset = index + ((articles.length - gallery.length) / 2);
                                return index < gallery.length ?
                                    <PressBlock 
                                        key={id}
                                        left={{
                                            type: "article",
                                            title: attributes.title,
                                            content: attributes.excerpt,
                                            link: attributes.link
                                        }}
                                        right={{
                                            type: "image",
                                            alt: gallery[index].attributes.alternativeText,
                                            src: `${process.env.NEXT_PUBLIC_IMG_URI}${gallery[index].attributes.url}`,
                                        }}
                                    /> : 
                                    <PressBlock 
                                        key={id}
                                        left={{
                                            type: "article",
                                            title: attributes.title,
                                            content: attributes.excerpt,
                                            link: attributes.link
                                        }}
                                        right={articles[offset] ? {
                                            type: "article",
                                            title: articles[offset].title,
                                            content: articles[offset].excerpt,
                                            link: articles[offset].link
                                        } : {type: null}}
                                    />
                            })            
                        }
                    </> : null 
                }
                {
                    articles.length <= gallery.length ? 
                    <>
                        <h4>Les photos du Fefan</h4>
                        <EditionGallery images={gallery.slice(articles.length)}/>
                    </> : null
                }
                </> : <Empty message="Il n'y a aucune information pour cette édition à afficher."/>
            }
        </main>
    );
}
