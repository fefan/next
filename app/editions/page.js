import getData from '@/api';
import styles from './style.module.scss'
import EditionElement from '@/components/editionElement';
import Empty from '@/components/empty';

export async function generateMetadata() {
    const site = await getData('site', {
        fields: ['description', 'author']
    });

    const data = await getData('editions', {
        populate: {
            gallery: {
                fields: ['name', 'url', 'alternativeText']
            },
            statistics: {
                fields: ['name', 'value']
            },
            flyer: {
                fields: ['name', 'url', 'alternativeText', 'width', 'height']
            }
        }
    });
    const editions = data.data;

    return site.data ? {
        metadataBase: `${process.env.NEXT_PUBLIC_ORIGIN}`,
        title: "Editions précédentes — Le Fefan",
        description: site.data.attributes.description,
        alternates: {
            canonical: '/editions',
        },
        openGraph: {
            title: "Editions précédentes — Le Fefan",
            url: `${process.env.NEXT_PUBLIC_ORIGIN}/prog/city-wide`,
            description: site.data.attributes.description, 
            images: editions.map(({attributes: attr}) => (
                {
                    width: attr.flyer.data.attributes.width,
                    height: attr.flyer.data.attributes.height,
                    alt: attr.flyer.data.attributes.alternativeText,
                    url: attr.flyer.data.attributes.url
                }
            )),
            authors: [site.data.attributes.author],
            type: 'website',
            locale: 'fr_FR',
            siteName: 'Le Fefan - Festival de Fanfares'
        }
    } : {}
}

export default async function Editions() {
    const site = await getData('site', {
        fields: ['id'],
        populate: {
            edition: {
                fields: ['id'],
            }
        }
    });

    const data = await getData('editions', {
        populate: {
            gallery: {
                fields: ['name', 'url', 'alternativeText']
            },
            statistics: {
                fields: ['name', 'value']
            },
            flyer: {
                fields: ['name', 'url', 'alternativeText']
            },
            fields: ['movie']
        }
    });

    const editions = (data.data ?? []).filter(e => e.id !== site.data?.attributes.edition.data?.id);

    return (
        <main className={styles.main}>
        {
            editions ? editions.map(({ id, attributes: attr }) => {
                const stats = attr.statistics.data.map(({id, attributes}) => ({id, type: 'stat', title: attributes.name, value: attributes.value}));
                const images = attr.gallery.data.map(({id, attributes}) => ({id, type: 'image', title: attributes.alternativeText, value: `${process.env.NEXT_PUBLIC_IMG_URI}${attributes.url}`}));
                const movie = attr.movie ? [{id: "movie", type: "video", title: `Aftermovie ${attr.title}`, value: attr.movie, mode: 'thumbnail'}] : null

                return (
                    <EditionElement 
                        full
                        key={id}
                        href={`/editions/${id}`}
                        title={attr.title}
                        blocks={movie ? stats.concat(movie).concat(images) : stats.concat(images)}
                        flyerImg={`${process.env.NEXT_PUBLIC_IMG_URI}${attr.flyer.data.attributes.url}`}
                        flyerAlt={attr.flyer.data.attributes.alternativeText}
                    />
                )
            }) : <Empty message="Il n'y a aucune édition à afficher."></Empty>      
        } 
        </main>
    )
}
