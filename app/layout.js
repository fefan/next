import './globals.css'
import Header from '@/components/header'
import Footer from '@/components/footer'

export const dynqmic = 'force-dynamic'

export const metadata = {
  generator: 'Next.js',
  applicationName: 'Le Fefan',
  keywords: ['Fefan', 'Festival', 'Fanfare', 'Cuivres', 'Villeurbanne', 'Evenement', 'Musique'],
  authors: [{ name: 'Girasol' }],
  creator: 'Girasol',
}

export default function RootLayout({ children }) {
  return (
    <html lang="fr">
      <head>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Krub:wght@500;700&family=Shadows+Into+Light&display=swap" />
      </head>
      <body>
        <Header />
        {children}
        <Footer />
      </body>
    </html>
  )
}
