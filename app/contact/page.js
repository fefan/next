import getData from "@/api";
import { BlocksRenderer } from "@strapi/blocks-react-renderer";
import styles from "./style.module.scss";
import Email from "@/components/email";

export async function generateMetadata() {
    return {
        metadataBase: `${process.env.NEXT_PUBLIC_ORIGIN}`,
        title: "Contactez nous !",
    }
}

export default async function Contact() {
    const site = await getData('site', {})

    const content = site.data?.attributes.contact_text
    return (
        <main className={styles.main}>
            <article>
                {content ? <BlocksRenderer content={content}/> : null}
            </article>
            {site.data?.attributes.contact_mail ? <Email></Email> : null}
        </main>
    );
}