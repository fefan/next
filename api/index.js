import qs from 'qs';

async function getData(path, query) {
    const queryString = qs.stringify(query);
    const res = await fetch(
        `${process.env.NEXT_PUBLIC_CONTENT_URI ?? 'https://content.fefan.fr/api'}/${path}?${queryString}`,
        { next: { revalidate: 3600 } },
    );
    
    return await res.json();
}

export default getData;

