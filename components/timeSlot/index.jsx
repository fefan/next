import { BlocksRenderer } from '@strapi/blocks-react-renderer';
import styles from './style.module.scss';

export default async function TimeSlot(props) {
    const date_begin = new Date(props.start)
    const date_end = new Date(props.end)
    const date_format = new Intl.DateTimeFormat('fr', {
          weekday: 'long',
          month: "long",
          day: "numeric"
        }).format(date_begin)

    return (
        <article className={styles.timeSlot}>
            <h4>
                {date_format} - {date_begin.getHours()}h
                {date_begin.getMinutes() !== 0 ? date_begin.getMinutes() : ""} à&nbsp;
                {date_end.getHours()}h{date_end.getMinutes() !== 0 ? date_end.getMinutes() : ""}
            </h4>
            <BlocksRenderer content={props.content}/>
        </article>
    );
}