import styles from './style.module.scss';

export default function Button({as, type, children, ...props}) {
    switch (as ?? 'a') {
        case 'a':
            return (
                <a
                    tabIndex="0"
                    {...props}
                    className={`${styles[type ?? 'secondary']} ${styles.button} not-a-link ${props.className}`}
                >
                    {children}
                </a>
            );
        case 'button':
            return (
                <button
                    tabIndex="0"
                    {...props}
                    className={`${styles[type ?? 'secondary']} ${styles.button} not-a-link ${props.className}`}
                >
                    {children}
                </button>
            );

    }
    return (null);
}