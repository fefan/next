import ImageBlock from '../imageBlock';
import EditionBlock from './editionBlock';
import styles from './style.module.scss';

export default async function EditionElement(props) {
    
    return (
        <section className={`${styles.editionSection} ${props.full ? styles.fullViewport : ''}`}>
            {props.href ?
                (
                    <a className={styles.image} href={props.href}>
                        <img src={props.flyerImg} alt={props.flyerAlt}/>
                    </a>
                ) : (
                    <ImageBlock className={styles.image} alt={props.flyerAlt} src={props.flyerImg} />
                )
            }
            {props.blocks.slice(0, 6).map(block => (
                <EditionBlock {...block} key={`${block.type}-${block.id}`} href={props.href} />
            ))}
            {props.full ? <aside className={styles.callToAction}>
                <a href={props.href}>
                    <h2>{props.title}</h2>
                    <svg width="40px" height="40px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6 12H18M18 12L13 7M18 12L13 17" stroke="var(--fefan-1)" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                    </svg>
                </a>
            </aside> : null}
        </section>
    );
}