import ImageBlock from "@/components/imageBlock";
import StatBlock from "@/components/statBlock";
import VideoBlock from "@/components/videoBlock";

import styles from './style.module.scss';

export default async function EditionBlock({type, ...props}) {
    
    switch (type) {
        case 'stat':
            return (
                <article className={styles.editionBlock}>
                    <StatBlock {...props} />
                </article>
            );

        case 'image':
            return (
                <article className={styles.editionBlock}>
                    <ImageBlock {...props} src={props.value} alt={props.title}/>
                </article>
            );

        case 'video':
            return (
                <article className={styles.editionBlock}>
                    <VideoBlock {...props} src={props.value} title={props.title}/>
                </article>
            );
    }
    return null;
}