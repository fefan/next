import styles from './style.module.scss';

export default function ImagePopup({toggleShowPopup, src, alt, ...props}) {
    return (
        <div className={styles.modal}>
            <button onClick={toggleShowPopup} className={styles.close}>
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="36"
                    height="36"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="1"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-x"
                >
                    <line x1="18" y1="6" x2="6" y2="18" />
                    <line x1="6" y1="6" x2="18" y2="18" />
                </svg>
            </button>
            <div onClick={toggleShowPopup} className={styles.imgModal} />
            <img
                {...props}
                src={src}
                alt={alt}
                className={styles.popupImage}
            />
        </div>
    );
}