import Button from "../button";
import styles from "./style.module.scss";

export default function Empty({message, ...props}) {
    return <section className={styles.empty}>
        <h2 {...props}>{message ?? "Il n'y a rien ici."}</h2>
        <Button
            as="a"
            type="secondary"
            href="/"
        >
            Retour à la page d&apos;accueil
        </Button>
    </section>
}