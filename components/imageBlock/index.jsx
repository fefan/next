'use client';
import { useState } from 'react';
import styles from './style.module.scss';
import ImagePopup from '../imagePopup';

export default function ImageBlock({alt, src, ...props}) {
    const [showPopup, setShowPopup] = useState(false);

    const toggleShowPopup = () => {
        setShowPopup(prev => !prev);
    }

    return (
        <>
            <img 
                {...props}
                onClick={toggleShowPopup}
                src={src} 
                alt={alt} 
                className={`${styles.imageBlock} ${props.className}`}
            />
            {
                showPopup ?
                <ImagePopup
                    toggleShowPopup={toggleShowPopup}
                    alt={alt}
                    src={src}
                />
                 : null
            }
        </>
    );
}