import styles from './style.module.scss';

export default async function StatBlock(props) {
    return (
        <a href={props.href} className={styles.statBox}>
            <span>{props.value}</span>
            <h3>{props.title}</h3>
        </a>
    );
}