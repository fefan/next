import ImageBlock from "../imageBlock";
import styles from "./style.module.scss";

export default function EditionGallery({images}) {
    return (
        <section className={styles.gallery}>
            {images.map(({id, attributes: attr}) => (
                <div className={styles.galleryImage} key={id}>
                    <ImageBlock
                        alt={attr.alternativeText}
                        src={`${process.env.NEXT_PUBLIC_IMG_URI}${attr.url}`}
                    />
                </div>
            ))}
        </section>
    );
}