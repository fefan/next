import ArticleBlock from "../articleBlock";
import ImageBlock from "../imageBlock";
import styles from "./style.module.scss";

export default function PressBlock({
    left: {type: leftType, ...left},
    right: {type: rightType, ...right},
    ...props
}) {
    return (
        <section {...props} className={styles.pressBlock}>
            {
                leftType ? leftType === 'article' ?
                <ArticleBlock {...left} className={styles.pressArticle}/> : 
                <ImageBlock {...left} className={styles.pressImage}/> : null
            }
            
            {
                rightType ? rightType === 'article' ? 
                <ArticleBlock {...right} className={styles.pressArticle}/> : 
                <ImageBlock {...right} className={styles.pressImage}/> : null
            }
        </section>
    );
}