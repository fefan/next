import Link from "next/link";
import styles from "./style.module.scss";

export default async function Footer() {
    return (
        <footer className={styles.footer}>
            <Link href="/legal">Mentions légales</Link>
            <span className={styles.separator}> · </span>
            <Link href="/sponsor">Ils nous soutiennent</Link>
            <span className={styles.separator}> · </span>
            <Link href="/contact">Contact</Link>
        </footer>
    );
}