'use client';

import qs from 'qs';
import { useEffect, useState } from "react";
import Button from "../button";
import styles from "./style.module.scss";

function fetchEmail(query) {
    const queryString = qs.stringify(query);
    return fetch(`${process.env.NEXT_PUBLIC_CONTENT_URI}/site?${queryString}`).then((data) => data.json());
}

export default function Email() {
    const [email, setEmail] = useState("");
    const [displayEmail, setDisplayEmail] = useState(false);
    const [copied, setCopied] = useState(false);

    const toggleEmail = () => {
        if (email === "") {
            fetchEmail({
                fields: ['contact_mail']
            }).then((res) => {
                setEmail(res.data.attributes.contact_mail);
                setDisplayEmail(true)
            })
            return;
        } else if (displayEmail === false) {
            setDisplayEmail(true)
        } else {
            navigator.clipboard.writeText(email);
            setCopied(true);
        }
    }

    useEffect(() => {
        setTimeout(() => {
            setDisplayEmail(false);
        }, 5000);
    }, [displayEmail])

    useEffect(() => {
        setTimeout(() => {
            setCopied(false);
        }, 2000);
    }, [copied])

    return (
        <Button
            as="button"
            type="secondary"
            onClick={toggleEmail}
            className={styles.emailButton}
            title={displayEmail ? "Cliquez pour copier le mail" : null}
        >
            <label className={displayEmail ? styles.copy : null}>
                {displayEmail ? email : "Cliquez pour afficher le mail"}
            </label>
            <label className={copied ? styles.showLabel : styles.hideLabel}>
                {copied ? "copié" : null}
            </label>
        </Button>
    );
}