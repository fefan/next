import styles from "./style.module.scss";

export default async function VideoBlock(props) {
    return (
        <iframe 
            className={`${props.className} ${styles.videoBlock}`}
            width={ props.mode ==='thumbnail' ? "220" : "560" } 
            height={props.mode ==='thumbnail' ? "220" : "315"}
            src={props.src}
            title={props.title} 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
            allowFullScreen
        />
    );
}