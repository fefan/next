import getData from '@/api';
import NavigationDrawer from './navigationDrawer';

export default async function Header() {
    const site = await getData('site', {
        populate: {
            edition: {
                fields: [],
                populate: {
                    programs: {
                        fields: ['type', 'title'],
                    }
                }
            }
        }
    });
    const editionId = site.data?.attributes.edition.data?.id;
    const programs = editionId ? site.data.attributes.edition.data.attributes.programs.data : null
    const socialLinkQuery = editionId ? {
        filters: {
            $or: [
                { edition: { id: { $null: true } } },
                { edition: { id: { $eq: editionId } } },
            ]
        }
    } : {
        filters: { edition: { id: { $null: true } } },
    }
    const socialLinks = await getData('social-links', socialLinkQuery)
    const links = socialLinks.data ?? []
    return (
        <NavigationDrawer programs={programs} links={links}/>
    );
}