"use client";
import Link from "next/link";
import SocialIcon from "../social-icon";
import { useState } from "react";
import styles from "./style.module.scss";

export default function NavigationDrawer({programs, links , ...props}) {
    const [isOpen, setIsOpen] = useState(false);

    const toggleDrawer = () => {
        setIsOpen(prev => !prev);
    }

    return (
        <header className={`${styles.header} ${isOpen ? styles.drawerOpen : ""}`}>
            <div className={`${styles.backdrop}`} onClick={toggleDrawer}/>
            <button className={`${styles.social} ${styles.burgerIcon}`} onClick={toggleDrawer}>
                <svg className={`feather feather-menu`} xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="#1B519E" strokeWidth="1" strokeLinecap="round" strokeLinejoin="round"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
            </button>
            <Link href="/" className={`not-a-link ${styles.logo}`}>
                <img src="/fefan.png" alt="Fefan" />
                <h1>Festival de fanfare</h1>
            </Link>
            <a className={`${styles.social} ${styles.programIcon}`} href="/prog/city-wide">
                <svg className={`feather feather-calendar`} xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="#1B519E" strokeWidth="1" strokeLinecap="round" strokeLinejoin="round"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
            </a>
            <nav className={`${styles.navigation}`}>
                <div className={styles.spacer} />
                <Link href="/">Accueil</Link>
                {programs ? programs.map(({ id, attributes: attr }) => (
                    <Link href={`/prog/${attr.type}`} key={id}>{attr.title}</Link>
                )): null}
                <Link href="/editions">Éditions précédentes</Link>
                <Link href="/contact">Contact</Link>
                <div className={styles.spacer} />
                <nav className={styles.socialLinks}>
                    {links.map(({ id, attributes: attr }) => (
                        <Link aria-label={attr.type} target="_blank" href={attr.uri} key={id} className={`${styles.social} not-a-link`}>
                            <SocialIcon name={attr.type} />
                        </Link>
                    ))}
                </nav>
            </nav>
        </header>
    );
}