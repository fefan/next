import { BlocksRenderer } from "@strapi/blocks-react-renderer";
import Button from "../button";
import styles from "./style.module.scss";

export default function ArticleBlock({title, content, link, className, ...props}) {
    return (
        <article {...props} className={`${styles.articleBlock} ${className ?? ''}`}>
            <h4>{title}</h4>
            {content ? <BlocksRenderer content={content}/> : null}
            <Button 
                type="secondary"
                as="a" 
                href={link}
                target="_blank"
            >
                Lire la suite
            </Button>
        </article>
    );
}
