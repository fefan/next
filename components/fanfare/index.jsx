import styles from './style.module.scss';

export default async function Fanfare(props) {
    return (
        <div className={styles.band}>
            <h4>{props.name}</h4>
            <h5>{props.location}</h5>
        </div>
    );
}